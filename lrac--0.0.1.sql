create function fn_trg_alter_table() returns event_trigger
    security definer
    language plpgsql
as
$$
DECLARE
    relation              RECORD;
    is_in_replication_set BOOLEAN := FALSE;
BEGIN
    FOR relation IN SELECT * FROM pg_event_trigger_ddl_commands()
        LOOP
            IF tg_tag = 'ALTER TABLE' THEN
                BEGIN
                    SELECT count(1) > 0 -- TRUE if the table is in a publication
                    INTO is_in_replication_set
                    FROM pg_publication p,
                         LATERAL pg_get_publication_tables(p.pubname::TEXT) gpt(relid)
                    WHERE gpt.relid = relation.objid;

                    IF (NOT is_in_replication_set) OR
                       (is_in_replication_set AND current_setting('replication.allow_dll') = 'true') THEN
                        RAISE NOTICE 'Operation allowed. % %',
                            tg_tag,
                            relation.object_identity;
                    ELSE
                        RAISE EXCEPTION
                            USING ERRCODE = 'C4509',
                                MESSAGE = 'Operation cancelled. You cannot modify the structure of a TABLE that is ' ||
                                          'in a PUBLICATION if the ''replication.allow_dll'' configuration is ''true''',
                                HINT = 'Run ''SELECT set_config(''replication.allow_dll'', ''true'', false);'' ' ||
                                       'and try again',
                                DETAIL = 'This table belongs to a publication, make sure the changes are applied at ' ||
                                         'the subscriber to avoid logical replication issues.';
                    END IF;
                exception
                    WHEN undefined_object THEN
                        RAISE exception USING
                            MESSAGE = 'Operation cancelled. Set replication.allow_dll configuration to ''true'' ' ||
                                      'if you want to modify the table',
                            HINT = 'Run ''SELECT set_config(''replication.allow_dll'', ''true'', false);'' ' ||
                                   'and try again',
                            DETAIL = 'Modifications of table structures belonging to a publication are denied by ' ||
                                     'default to avoid logical replication issues.';
                END;
            END IF;
        END LOOP;
END;

$$;

CREATE EVENT TRIGGER trg_mv_info ON ddl_command_end
    WHEN TAG IN ('ALTER TABLE')
EXECUTE PROCEDURE fn_trg_alter_table();
