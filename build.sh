#!/bin/bash

CMDLINE="$*"
PROGNAME="$0"
BASENAME=`basename $PROGNAME`
YEAR=$(date +%Y)
APPLY=""
VERSION="0.0.1"
GITLAB_CI=".gitlab-ci.yml"

files="
README.template
.gitlab-ci.yml
CONTRIBUTING.md
LICENSE
Makefile
"

function usage {
    cat <<EOF
Usage:
        $BASENAME [options]

Options:
-e name     extension name
-v version  extension version, default $VERSION
-i          apply changes
-h|-?       show this help

EOF
    exit 1
}

if [[ ${#} -eq 0 ]]; then
    usage
fi

while getopts ":e:v:ih?" flag; do
    case $flag in
        e)      EXTENSION="$OPTARG";;
        v)      VERSION="$OPTARG";;
        i)      APPLY="-i";;
        h|\?)
                if [ "$CMDLINE" != "-?" -a "$CMDLINE" != -h ]; then
                    echo "Illegal command:"
                    echo "      $0 $CMDLINE"
                    echo
                fi
                usage;;

        *)
            usage
            ;;
    esac
done

sed ${APPLY} 's/{{EXTENSION}}/'''${EXTENSION}'''/g' $files
sed ${APPLY} 's/{{YEAR}}/'''${YEAR}'''/g' $files
sed ${APPLY} 's/{{VERSION}}/'''${VERSION}'''/g' $files
mv ${GITLAB_CI}.template ${GITLAB_CI}
