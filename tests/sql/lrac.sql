CREATE EXTENSION lrac schema public;

CREATE TABLE foo();

-- tests to add a column to a table belonging to a publication
-- result Exception
CREATE PUBLICATION pub_foo FOR TABLE foo;
ALTER TABLE foo ADD COLUMN i INTEGER;

-- tests that the table structure is allowed to be modified
-- result Success
SELECT set_config('replication.allow_dll', 'true', false);
ALTER TABLE foo ADD COLUMN i INTEGER;

-- tests that the table structure cannot be modified if replication.allow_dll is not true
-- result Exception
SELECT set_config('replication.allow_dll', 'false', false);
ALTER TABLE foo ADD COLUMN fail text;

-- removing the table from publication, tests that the table structure can be modified
-- result Success
ALTER PUBLICATION pub_foo drop table foo ;
ALTER TABLE foo ADD COLUMN t text;

DROP table foo;
DROP PUBLICATION pub_foo;
