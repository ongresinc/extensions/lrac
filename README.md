# mv_stats extension

This extension prevents by default a user from changing the structure of one or 
more tables belonging to one or more publications. This behavior is controlled by 
`replication.allow_dll` configuration. See the [Example of use])(#example-of-use) 
section.

## Install

*Required PG10+ (tested)* 

Run: 

```sh
make install
```

If not install, you must make sure you can see the binary pg_config,
maybe setting PostgreSQL binary path in the OS or setting PG_CONFIG = /path_to_pg_config/ 
in the makefile or run: `make install PG_CONFIG=/path_to_pg_config/`

In your database execute: 

```sql
CREATE EXTENSION lrac;
```

## Example of use

Create a table and add it to a publication and try to modify its structure

```sql
test=# CREATE PUBLICATION pub1;
CREATE PUBLICATION
test=# CREATE TABLE foo();
CREATE TABLE
test=# ALTER PUBLICATION pub1 ADD TABLE foo;
ALTER PUBLICATION
test=# ALTER TABLE foo ADD COLUMN i INTEGER;
ERROR:  Operation cancelled. Set replication.allow_dll configuration to 'true' if you want to modify the table
DETAIL:  Modifications of table structures belonging to a publication are denied by default to avoid logical replication issues.
HINT:  Run 'SELECT set_config('replication.allow_dll', 'true', false);' and try again
CONTEXT:  PL/pgSQL function fn_trg_alter_table() line 33 at RAISE
sandbox=#
```

To allow modifying the structure of a table set `replication.allow_dll` to true

```sql
test=# SELECT set_config('replication.allow_dll', 'true', false);
 set_config 
------------
 true
(1 row)

test=# ALTER TABLE foo ADD COLUMN i INTEGER;
NOTICE:  ALTER TABLE public.foo
ALTER TABLE
```

IMPORTANT: If you find some bugs in the existing version, please contact to me.

[Ongres contact](mailto:dbonne@ongres.com)
[Personal contact](mailto:7658145+dbonne@users.noreply.github.com)
